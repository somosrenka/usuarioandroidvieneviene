package com.somosrenka.developers.functions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonDatosCarro
{
    public JSONObject jObject;

    public JsonDatosCarro(String json) throws JSONException
    { jObject = new JSONObject(json); }

    public boolean has_success() throws JSONException
    { return jObject.has("key"); }

    public String getToken() throws JSONException
    { return jObject.getString("key"); }

    public String getError() throws JSONException
    { return jObject.optString("detail"); }

    public List<String> getErrors() throws JSONException
    {
        ArrayList<String> array = new ArrayList<>();
        array.add(jObject.optString("placas"));
        array.add(jObject.optString("color"));
        array.add(jObject.optString("marca"));
        return array;
    }


}
